"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var example_1 = require("./example");
var fast_check_1 = require("fast-check");
describe('Natural numbers:', function () {
    // string text always contains itself
    it('should be equal to itself', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
});
describe('Objects3D:', function () {
    // string text always contains itself
    it('should have type of THREE.Object3D', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.Obj3dSpecCheck.selfequalprop());
    });
    it('should translate properly on X', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
    it('should translate properly on Y', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
    it('should translate properly on Z', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
    it('should rotate properly on X', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
    it('should rotate properly on Y', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
    it('should rotate properly on Z', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
    it('euler and back quaternion rotation should be roundtrip', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
    it('euler and back matrice rotation should be roundtrip', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
    it('should extend properly', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
});
describe('BoxGeometry:', function () {
    // string text always contains itself
    it('should be Object3D', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
    it('should be serializable/deserializable', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
    it('should be clonable', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
});
describe('Math:', function () {
    // string text always contains itself
    it('Eulers are correct', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
    it('Matrice3 are correct', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
    it('Quaternions are correct', function () {
        fast_check_1.default.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        example_1.MySpecCheck.predsuccprop());
    });
});
//Should translate properly
//Should rotate properly
//Should be properly rotated by euler angles as with quaternions as with matrices
//Should be properly extended by another object3D
//Any geometry is Object3D
//Box geometry(Any geometry) should be serializable and deserealizsable
//Should be clonable 
//Eulers work as planned
//Matrice3 work as planned
//Quaternions work as planned 
// QUnit.test( 'type', ( assert ) => {
//   const object = new Object3D();
//   assert.ok(
//     object.type === 'Object3D',
//     'Object3D.type should be Object3D'
//   );
// } );
