import {MySpecCheck,Obj3dSpecCheck} from "./example"
import fc from 'fast-check';


describe('Natural numbers:', () => {
    // string text always contains itself
    it('should be equal to itself', () => {
      fc.assert(
        //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
        MySpecCheck.predsuccprop()

      );
    });
})

describe('Objects3D:', () => {
  // string text always contains itself
  it('should have type of THREE.Object3D', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      Obj3dSpecCheck.selfequalprop()
    );
  });
  it('should translate properly on X', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()

    );
  });
  it('should translate properly on Y', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()

    );
  });
  it('should translate properly on Z', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()

    );
  });
  it('should rotate properly on X', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()

    );
  });
  it('should rotate properly on Y', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()

    );
  });
  it('should rotate properly on Z', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()

    );
  });
  it('euler and back quaternion rotation should be roundtrip', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()

    );
  });
  it('euler and back matrice rotation should be roundtrip', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()

    );
  });
  it('should extend properly', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()

    );
  });

})

describe('BoxGeometry:', () => {
  // string text always contains itself
  it('should be Object3D', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()
    );
  });

  it('should be serializable/deserializable', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()
    );
  });

  it('should be clonable', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()
    );
  });


})

describe('Math:', () => {
  // string text always contains itself
  it('Eulers are correct', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()
    );
  });

  it('Matrice3 are correct', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()
    );
  });

  it('Quaternions are correct', () => {
    fc.assert(
      //Свойство принимает на вход арбитрарные переменные (В данном случае, типа integer)
      MySpecCheck.predsuccprop()
    );
  });


})


//Should translate properly
//Should rotate properly
//Should be properly rotated by euler angles as with quaternions as with matrices
//Should be properly extended by another object3D

//Any geometry is Object3D
//Box geometry(Any geometry) should be serializable and deserealizsable
//Should be clonable 

//Eulers work as planned
//Matrice3 work as planned
//Quaternions work as planned 

// QUnit.test( 'type', ( assert ) => {

//   const object = new Object3D();
//   assert.ok(
//     object.type === 'Object3D',
//     'Object3D.type should be Object3D'
//   );

// } );